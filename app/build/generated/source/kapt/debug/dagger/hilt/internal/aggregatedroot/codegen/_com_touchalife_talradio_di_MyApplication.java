package dagger.hilt.internal.aggregatedroot.codegen;

import dagger.hilt.android.HiltAndroidApp;
import dagger.hilt.internal.aggregatedroot.AggregatedRoot;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedRoot(
    root = "com.touchalife.talradio.di.MyApplication",
    originatingRoot = "com.touchalife.talradio.di.MyApplication",
    rootAnnotation = HiltAndroidApp.class
)
public class _com_touchalife_talradio_di_MyApplication {
}
