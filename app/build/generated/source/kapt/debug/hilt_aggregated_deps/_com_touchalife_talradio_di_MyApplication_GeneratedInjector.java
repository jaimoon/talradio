package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.components.SingletonComponent",
    entryPoints = "com.touchalife.talradio.di.MyApplication_GeneratedInjector"
)
public class _com_touchalife_talradio_di_MyApplication_GeneratedInjector {
}
