package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ActivityComponent",
    entryPoints = "com.touchalife.talradio.ui.LoginActivity_GeneratedInjector"
)
public class _com_touchalife_talradio_ui_LoginActivity_GeneratedInjector {
}
