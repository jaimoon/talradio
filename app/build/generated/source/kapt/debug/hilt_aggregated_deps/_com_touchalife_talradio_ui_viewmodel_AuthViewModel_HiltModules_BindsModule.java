package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ViewModelComponent",
    modules = "com.touchalife.talradio.ui.viewmodel.AuthViewModel_HiltModules.BindsModule"
)
public class _com_touchalife_talradio_ui_viewmodel_AuthViewModel_HiltModules_BindsModule {
}
