1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="com.touchalife.talradio"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="23"
8-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml
9        android:targetSdkVersion="31" />
9-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml
10
11    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
11-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:5:5-79
11-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:5:22-76
12    <uses-permission android:name="android.permission.INTERNET" />
12-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:6:5-67
12-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:6:22-64
13    <uses-permission android:name="android.permission.WAKE_LOCK" />
13-->[com.google.android.gms:play-services-measurement-api:19.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\31cc59ab7ad239993b2d9d716e834077\transformed\jetified-play-services-measurement-api-19.0.1\AndroidManifest.xml:24:5-68
13-->[com.google.android.gms:play-services-measurement-api:19.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\31cc59ab7ad239993b2d9d716e834077\transformed\jetified-play-services-measurement-api-19.0.1\AndroidManifest.xml:24:22-65
14    <uses-permission android:name="com.google.android.finsky.permission.BIND_GET_INSTALL_REFERRER_SERVICE" />
14-->[com.google.android.gms:play-services-measurement:19.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\66f622ece99e9fc63740e4384bc85c8f\transformed\jetified-play-services-measurement-19.0.1\AndroidManifest.xml:26:5-110
14-->[com.google.android.gms:play-services-measurement:19.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\66f622ece99e9fc63740e4384bc85c8f\transformed\jetified-play-services-measurement-19.0.1\AndroidManifest.xml:26:22-107
15
16    <application
16-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:8:5-67:19
17        android:name="com.touchalife.talradio.di.MyApplication"
17-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:9:9-41
18        android:allowBackup="true"
18-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:10:9-35
19        android:appComponentFactory="androidx.core.app.CoreComponentFactory"
19-->[androidx.core:core:1.6.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\38fc408269900ec0b88312625c813bd5\transformed\core-1.6.0\AndroidManifest.xml:24:18-86
20        android:debuggable="true"
21        android:extractNativeLibs="false"
22        android:icon="@mipmap/ic_launcher"
22-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:11:9-43
23        android:label="@string/app_name"
23-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:12:9-41
24        android:roundIcon="@mipmap/ic_launcher_round"
24-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:13:9-54
25        android:supportsRtl="true"
25-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:14:9-35
26        android:testOnly="true"
27        android:theme="@style/Theme.TALRadio"
27-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:15:9-46
28        android:usesCleartextTraffic="true" >
28-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:16:9-44
29        <activity
29-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:17:9-20:59
30            android:name="com.touchalife.talradio.ui.HomeActivity"
30-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:18:13-44
31            android:exported="true"
31-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:19:13-36
32            android:label="@string/title_activity_home" />
32-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:20:13-56
33        <activity
33-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:21:9-23:39
34            android:name="com.touchalife.talradio.ui.ResetActivity"
34-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:22:13-45
35            android:exported="true" />
35-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:23:13-36
36        <activity
36-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:24:9-27:50
37            android:name="com.touchalife.talradio.ui.RegistrationActivity"
37-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:25:13-52
38            android:exported="true"
38-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:26:13-36
39            android:theme="@style/noActionBar" />
39-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:27:13-47
40        <activity
40-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:28:9-32:63
41            android:name="com.touchalife.talradio.ui.LoginActivity"
41-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:29:13-45
42            android:exported="true"
42-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:30:13-36
43            android:theme="@style/noActionBar"
43-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:31:13-47
44            android:windowSoftInputMode="stateAlwaysHidden" />
44-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:32:13-60
45        <activity
45-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:33:9-42:20
46            android:name="com.touchalife.talradio.ui.SplashActivity"
46-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:34:13-46
47            android:exported="true"
47-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:35:13-36
48            android:theme="@style/noActionBar" >
48-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:36:13-47
49            <intent-filter>
49-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:37:13-41:29
50                <action android:name="android.intent.action.MAIN" />
50-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:38:17-69
50-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:38:25-66
51
52                <category android:name="android.intent.category.LAUNCHER" />
52-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:40:17-77
52-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:40:27-74
53            </intent-filter>
54        </activity>
55        <activity
55-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:43:9-45:39
56            android:name="com.touchalife.talradio.ui.MainActivity"
56-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:44:13-44
57            android:exported="true" />
57-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:45:13-36
58
59        <meta-data
59-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:47:9-49:55
60            android:name="com.facebook.sdk.ApplicationId"
60-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:48:13-58
61            android:value="@string/facebook_app_id" />
61-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:49:13-52
62
63        <activity
63-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:51:9-54:48
64            android:name="com.facebook.FacebookActivity"
64-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:52:13-57
65            android:configChanges="keyboard|keyboardHidden|screenLayout|screenSize|orientation"
65-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:53:13-96
66            android:label="@string/app_name"
66-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:54:13-45
67            android:theme="@style/com_facebook_activity_theme" />
67-->[com.facebook.android:facebook-common:5.15.3] D:\Downloads\gradle-6.5-bin\caches\transforms-3\3420f25c46a10d86fd62948cdb616c48\transformed\jetified-facebook-common-5.15.3\AndroidManifest.xml:33:13-63
68        <activity
68-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:55:9-66:20
69            android:name="com.facebook.CustomTabActivity"
69-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:56:13-58
70            android:exported="true" >
70-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:57:13-36
71            <intent-filter>
71-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:58:13-65:29
72                <action android:name="android.intent.action.VIEW" />
72-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:59:17-69
72-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:59:25-66
73
74                <category android:name="android.intent.category.DEFAULT" />
74-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:61:17-76
74-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:61:27-73
75                <category android:name="android.intent.category.BROWSABLE" />
75-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:62:17-78
75-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:62:27-75
76
77                <data android:scheme="@string/fb_login_protocol_scheme" />
77-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:64:17-75
77-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:64:23-72
78            </intent-filter>
79            <intent-filter>
79-->[com.facebook.android:facebook-common:5.15.3] D:\Downloads\gradle-6.5-bin\caches\transforms-3\3420f25c46a10d86fd62948cdb616c48\transformed\jetified-facebook-common-5.15.3\AndroidManifest.xml:39:13-48:29
80                <action android:name="android.intent.action.VIEW" />
80-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:59:17-69
80-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:59:25-66
81
82                <category android:name="android.intent.category.DEFAULT" />
82-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:61:17-76
82-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:61:27-73
83                <category android:name="android.intent.category.BROWSABLE" />
83-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:62:17-78
83-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:62:27-75
84
85                <data
85-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:64:17-75
86                    android:host="cct.com.touchalife.talradio"
87                    android:scheme="fbconnect" />
87-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:64:23-72
88            </intent-filter>
89        </activity>
90        <activity android:name="com.facebook.CustomTabMainActivity" />
90-->[com.facebook.android:facebook-common:5.15.3] D:\Downloads\gradle-6.5-bin\caches\transforms-3\3420f25c46a10d86fd62948cdb616c48\transformed\jetified-facebook-common-5.15.3\AndroidManifest.xml:34:9-71
90-->[com.facebook.android:facebook-common:5.15.3] D:\Downloads\gradle-6.5-bin\caches\transforms-3\3420f25c46a10d86fd62948cdb616c48\transformed\jetified-facebook-common-5.15.3\AndroidManifest.xml:34:19-68
91        <activity
91-->[com.google.android.gms:play-services-auth:19.2.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\3ffd8b3ba704462beae3a4a4130656fd\transformed\jetified-play-services-auth-19.2.0\AndroidManifest.xml:23:9-27:75
92            android:name="com.google.android.gms.auth.api.signin.internal.SignInHubActivity"
92-->[com.google.android.gms:play-services-auth:19.2.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\3ffd8b3ba704462beae3a4a4130656fd\transformed\jetified-play-services-auth-19.2.0\AndroidManifest.xml:24:13-93
93            android:excludeFromRecents="true"
93-->[com.google.android.gms:play-services-auth:19.2.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\3ffd8b3ba704462beae3a4a4130656fd\transformed\jetified-play-services-auth-19.2.0\AndroidManifest.xml:25:13-46
94            android:exported="false"
94-->[com.google.android.gms:play-services-auth:19.2.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\3ffd8b3ba704462beae3a4a4130656fd\transformed\jetified-play-services-auth-19.2.0\AndroidManifest.xml:26:13-37
95            android:theme="@android:style/Theme.Translucent.NoTitleBar" />
95-->[com.google.android.gms:play-services-auth:19.2.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\3ffd8b3ba704462beae3a4a4130656fd\transformed\jetified-play-services-auth-19.2.0\AndroidManifest.xml:27:13-72
96        <!--
97            Service handling Google Sign-In user revocation. For apps that do not integrate with
98            Google Sign-In, this service will never be started.
99        -->
100        <service
100-->[com.google.android.gms:play-services-auth:19.2.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\3ffd8b3ba704462beae3a4a4130656fd\transformed\jetified-play-services-auth-19.2.0\AndroidManifest.xml:33:9-37:51
101            android:name="com.google.android.gms.auth.api.signin.RevocationBoundService"
101-->[com.google.android.gms:play-services-auth:19.2.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\3ffd8b3ba704462beae3a4a4130656fd\transformed\jetified-play-services-auth-19.2.0\AndroidManifest.xml:34:13-89
102            android:exported="true"
102-->[com.google.android.gms:play-services-auth:19.2.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\3ffd8b3ba704462beae3a4a4130656fd\transformed\jetified-play-services-auth-19.2.0\AndroidManifest.xml:35:13-36
103            android:permission="com.google.android.gms.auth.api.signin.permission.REVOCATION_NOTIFICATION"
103-->[com.google.android.gms:play-services-auth:19.2.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\3ffd8b3ba704462beae3a4a4130656fd\transformed\jetified-play-services-auth-19.2.0\AndroidManifest.xml:36:13-107
104            android:visibleToInstantApps="true" />
104-->[com.google.android.gms:play-services-auth:19.2.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\3ffd8b3ba704462beae3a4a4130656fd\transformed\jetified-play-services-auth-19.2.0\AndroidManifest.xml:37:13-48
105
106        <activity
106-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:27:9-44:20
107            android:name="com.google.firebase.auth.internal.GenericIdpActivity"
107-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:28:13-80
108            android:excludeFromRecents="true"
108-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:29:13-46
109            android:exported="true"
109-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:30:13-36
110            android:launchMode="singleTask"
110-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:31:13-44
111            android:theme="@android:style/Theme.Translucent.NoTitleBar" >
111-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:32:13-72
112            <intent-filter>
112-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:33:13-43:29
113                <action android:name="android.intent.action.VIEW" />
113-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:59:17-69
113-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:59:25-66
114
115                <category android:name="android.intent.category.DEFAULT" />
115-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:61:17-76
115-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:61:27-73
116                <category android:name="android.intent.category.BROWSABLE" />
116-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:62:17-78
116-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:62:27-75
117
118                <data
118-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:64:17-75
119                    android:host="firebase.auth"
120                    android:path="/"
121                    android:scheme="genericidp" />
121-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:64:23-72
122            </intent-filter>
123        </activity>
124        <activity
124-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:45:9-62:20
125            android:name="com.google.firebase.auth.internal.RecaptchaActivity"
125-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:46:13-79
126            android:excludeFromRecents="true"
126-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:47:13-46
127            android:exported="true"
127-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:48:13-36
128            android:launchMode="singleTask"
128-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:49:13-44
129            android:theme="@android:style/Theme.Translucent.NoTitleBar" >
129-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:50:13-72
130            <intent-filter>
130-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:51:13-61:29
131                <action android:name="android.intent.action.VIEW" />
131-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:59:17-69
131-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:59:25-66
132
133                <category android:name="android.intent.category.DEFAULT" />
133-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:61:17-76
133-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:61:27-73
134                <category android:name="android.intent.category.BROWSABLE" />
134-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:62:17-78
134-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:62:27-75
135
136                <data
136-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:64:17-75
137                    android:host="firebase.auth"
138                    android:path="/"
139                    android:scheme="recaptcha" />
139-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:64:23-72
140            </intent-filter>
141        </activity>
142
143        <service
143-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:64:9-73:19
144            android:name="com.google.firebase.auth.api.fallback.service.FirebaseAuthFallbackService"
144-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:65:13-101
145            android:enabled="true"
145-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:66:13-35
146            android:exported="false" >
146-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:67:13-37
147            <intent-filter>
147-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:68:13-72:29
148                <action android:name="com.google.firebase.auth.api.gms.service.START" />
148-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:69:17-89
148-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:69:25-86
149
150                <category android:name="android.intent.category.DEFAULT" />
150-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:61:17-76
150-->E:\Android projects\talradio\app\src\main\AndroidManifest.xml:61:27-73
151            </intent-filter>
152        </service>
153        <service
153-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:74:9-80:19
154            android:name="com.google.firebase.components.ComponentDiscoveryService"
154-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:75:13-84
155            android:directBootAware="true"
155-->[com.google.firebase:firebase-common:20.0.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\61039433608b88eb803fce931a756cd7\transformed\jetified-firebase-common-20.0.0\AndroidManifest.xml:35:13-43
156            android:exported="false" >
156-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:76:13-37
157            <meta-data
157-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:77:13-79:85
158                android:name="com.google.firebase.components:com.google.firebase.auth.FirebaseAuthRegistrar"
158-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:78:17-109
159                android:value="com.google.firebase.components.ComponentRegistrar" />
159-->[com.google.firebase:firebase-auth:21.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\ceaa69a56038b86df1d8f0c900efdd83\transformed\jetified-firebase-auth-21.0.1\AndroidManifest.xml:79:17-82
160            <meta-data
160-->[com.google.android.gms:play-services-measurement-api:19.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\31cc59ab7ad239993b2d9d716e834077\transformed\jetified-play-services-measurement-api-19.0.1\AndroidManifest.xml:30:13-32:85
161                android:name="com.google.firebase.components:com.google.firebase.analytics.connector.internal.AnalyticsConnectorRegistrar"
161-->[com.google.android.gms:play-services-measurement-api:19.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\31cc59ab7ad239993b2d9d716e834077\transformed\jetified-play-services-measurement-api-19.0.1\AndroidManifest.xml:31:17-139
162                android:value="com.google.firebase.components.ComponentRegistrar" />
162-->[com.google.android.gms:play-services-measurement-api:19.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\31cc59ab7ad239993b2d9d716e834077\transformed\jetified-play-services-measurement-api-19.0.1\AndroidManifest.xml:32:17-82
163            <meta-data
163-->[com.google.firebase:firebase-installations:17.0.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\c1c48ce528facf63624dc4043b73ec9a\transformed\jetified-firebase-installations-17.0.0\AndroidManifest.xml:18:13-20:85
164                android:name="com.google.firebase.components:com.google.firebase.installations.FirebaseInstallationsRegistrar"
164-->[com.google.firebase:firebase-installations:17.0.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\c1c48ce528facf63624dc4043b73ec9a\transformed\jetified-firebase-installations-17.0.0\AndroidManifest.xml:19:17-127
165                android:value="com.google.firebase.components.ComponentRegistrar" />
165-->[com.google.firebase:firebase-installations:17.0.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\c1c48ce528facf63624dc4043b73ec9a\transformed\jetified-firebase-installations-17.0.0\AndroidManifest.xml:20:17-82
166        </service>
167
168        <activity
168-->[com.google.android.gms:play-services-base:17.5.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\1e04efd0021dec5c9a2ac33c94112833\transformed\jetified-play-services-base-17.5.0\AndroidManifest.xml:23:9-26:75
169            android:name="com.google.android.gms.common.api.GoogleApiActivity"
169-->[com.google.android.gms:play-services-base:17.5.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\1e04efd0021dec5c9a2ac33c94112833\transformed\jetified-play-services-base-17.5.0\AndroidManifest.xml:24:13-79
170            android:exported="false"
170-->[com.google.android.gms:play-services-base:17.5.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\1e04efd0021dec5c9a2ac33c94112833\transformed\jetified-play-services-base-17.5.0\AndroidManifest.xml:25:13-37
171            android:theme="@android:style/Theme.Translucent.NoTitleBar" />
171-->[com.google.android.gms:play-services-base:17.5.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\1e04efd0021dec5c9a2ac33c94112833\transformed\jetified-play-services-base-17.5.0\AndroidManifest.xml:26:13-72
172
173        <provider
173-->[com.google.firebase:firebase-common:20.0.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\61039433608b88eb803fce931a756cd7\transformed\jetified-firebase-common-20.0.0\AndroidManifest.xml:26:9-31:39
174            android:name="com.google.firebase.provider.FirebaseInitProvider"
174-->[com.google.firebase:firebase-common:20.0.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\61039433608b88eb803fce931a756cd7\transformed\jetified-firebase-common-20.0.0\AndroidManifest.xml:27:13-77
175            android:authorities="com.touchalife.talradio.firebaseinitprovider"
175-->[com.google.firebase:firebase-common:20.0.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\61039433608b88eb803fce931a756cd7\transformed\jetified-firebase-common-20.0.0\AndroidManifest.xml:28:13-72
176            android:directBootAware="true"
176-->[com.google.firebase:firebase-common:20.0.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\61039433608b88eb803fce931a756cd7\transformed\jetified-firebase-common-20.0.0\AndroidManifest.xml:29:13-43
177            android:exported="false"
177-->[com.google.firebase:firebase-common:20.0.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\61039433608b88eb803fce931a756cd7\transformed\jetified-firebase-common-20.0.0\AndroidManifest.xml:30:13-37
178            android:initOrder="100" />
178-->[com.google.firebase:firebase-common:20.0.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\61039433608b88eb803fce931a756cd7\transformed\jetified-firebase-common-20.0.0\AndroidManifest.xml:31:13-36
179
180        <receiver
180-->[com.google.android.gms:play-services-measurement:19.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\66f622ece99e9fc63740e4384bc85c8f\transformed\jetified-play-services-measurement-19.0.1\AndroidManifest.xml:29:9-33:20
181            android:name="com.google.android.gms.measurement.AppMeasurementReceiver"
181-->[com.google.android.gms:play-services-measurement:19.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\66f622ece99e9fc63740e4384bc85c8f\transformed\jetified-play-services-measurement-19.0.1\AndroidManifest.xml:30:13-85
182            android:enabled="true"
182-->[com.google.android.gms:play-services-measurement:19.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\66f622ece99e9fc63740e4384bc85c8f\transformed\jetified-play-services-measurement-19.0.1\AndroidManifest.xml:31:13-35
183            android:exported="false" >
183-->[com.google.android.gms:play-services-measurement:19.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\66f622ece99e9fc63740e4384bc85c8f\transformed\jetified-play-services-measurement-19.0.1\AndroidManifest.xml:32:13-37
184        </receiver>
185
186        <service
186-->[com.google.android.gms:play-services-measurement:19.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\66f622ece99e9fc63740e4384bc85c8f\transformed\jetified-play-services-measurement-19.0.1\AndroidManifest.xml:35:9-38:40
187            android:name="com.google.android.gms.measurement.AppMeasurementService"
187-->[com.google.android.gms:play-services-measurement:19.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\66f622ece99e9fc63740e4384bc85c8f\transformed\jetified-play-services-measurement-19.0.1\AndroidManifest.xml:36:13-84
188            android:enabled="true"
188-->[com.google.android.gms:play-services-measurement:19.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\66f622ece99e9fc63740e4384bc85c8f\transformed\jetified-play-services-measurement-19.0.1\AndroidManifest.xml:37:13-35
189            android:exported="false" />
189-->[com.google.android.gms:play-services-measurement:19.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\66f622ece99e9fc63740e4384bc85c8f\transformed\jetified-play-services-measurement-19.0.1\AndroidManifest.xml:38:13-37
190        <service
190-->[com.google.android.gms:play-services-measurement:19.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\66f622ece99e9fc63740e4384bc85c8f\transformed\jetified-play-services-measurement-19.0.1\AndroidManifest.xml:39:9-43:72
191            android:name="com.google.android.gms.measurement.AppMeasurementJobService"
191-->[com.google.android.gms:play-services-measurement:19.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\66f622ece99e9fc63740e4384bc85c8f\transformed\jetified-play-services-measurement-19.0.1\AndroidManifest.xml:40:13-87
192            android:enabled="true"
192-->[com.google.android.gms:play-services-measurement:19.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\66f622ece99e9fc63740e4384bc85c8f\transformed\jetified-play-services-measurement-19.0.1\AndroidManifest.xml:41:13-35
193            android:exported="false"
193-->[com.google.android.gms:play-services-measurement:19.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\66f622ece99e9fc63740e4384bc85c8f\transformed\jetified-play-services-measurement-19.0.1\AndroidManifest.xml:42:13-37
194            android:permission="android.permission.BIND_JOB_SERVICE" />
194-->[com.google.android.gms:play-services-measurement:19.0.1] D:\Downloads\gradle-6.5-bin\caches\transforms-3\66f622ece99e9fc63740e4384bc85c8f\transformed\jetified-play-services-measurement-19.0.1\AndroidManifest.xml:43:13-69
195
196        <meta-data
196-->[com.google.android.gms:play-services-basement:17.5.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\c84cef1ba3ab7bc293f5872214905293\transformed\jetified-play-services-basement-17.5.0\AndroidManifest.xml:23:9-25:69
197            android:name="com.google.android.gms.version"
197-->[com.google.android.gms:play-services-basement:17.5.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\c84cef1ba3ab7bc293f5872214905293\transformed\jetified-play-services-basement-17.5.0\AndroidManifest.xml:24:13-58
198            android:value="@integer/google_play_services_version" />
198-->[com.google.android.gms:play-services-basement:17.5.0] D:\Downloads\gradle-6.5-bin\caches\transforms-3\c84cef1ba3ab7bc293f5872214905293\transformed\jetified-play-services-basement-17.5.0\AndroidManifest.xml:25:13-66
199        <!--
200         The initialization ContentProvider will call FacebookSdk.sdkInitialize automatically
201         with the application context. This config is merged in with the host app's manifest,
202         but there can only be one provider with the same authority activated at any given
203         point; so if the end user has two or more different apps that use Facebook SDK, only the
204         first one will be able to use the provider. To work around this problem, we use the
205         following placeholder in the authority to identify each host application as if it was
206         a completely different provider.
207        -->
208        <provider
208-->[com.facebook.android:facebook-core:5.15.3] D:\Downloads\gradle-6.5-bin\caches\transforms-3\9f8ebb556426a803d22b2923460835c8\transformed\jetified-facebook-core-5.15.3\AndroidManifest.xml:39:9-42:40
209            android:name="com.facebook.internal.FacebookInitProvider"
209-->[com.facebook.android:facebook-core:5.15.3] D:\Downloads\gradle-6.5-bin\caches\transforms-3\9f8ebb556426a803d22b2923460835c8\transformed\jetified-facebook-core-5.15.3\AndroidManifest.xml:40:13-70
210            android:authorities="com.touchalife.talradio.FacebookInitProvider"
210-->[com.facebook.android:facebook-core:5.15.3] D:\Downloads\gradle-6.5-bin\caches\transforms-3\9f8ebb556426a803d22b2923460835c8\transformed\jetified-facebook-core-5.15.3\AndroidManifest.xml:41:13-72
211            android:exported="false" />
211-->[com.facebook.android:facebook-core:5.15.3] D:\Downloads\gradle-6.5-bin\caches\transforms-3\9f8ebb556426a803d22b2923460835c8\transformed\jetified-facebook-core-5.15.3\AndroidManifest.xml:42:13-37
212
213        <receiver
213-->[com.facebook.android:facebook-core:5.15.3] D:\Downloads\gradle-6.5-bin\caches\transforms-3\9f8ebb556426a803d22b2923460835c8\transformed\jetified-facebook-core-5.15.3\AndroidManifest.xml:44:9-50:20
214            android:name="com.facebook.CurrentAccessTokenExpirationBroadcastReceiver"
214-->[com.facebook.android:facebook-core:5.15.3] D:\Downloads\gradle-6.5-bin\caches\transforms-3\9f8ebb556426a803d22b2923460835c8\transformed\jetified-facebook-core-5.15.3\AndroidManifest.xml:45:13-86
215            android:exported="false" >
215-->[com.facebook.android:facebook-core:5.15.3] D:\Downloads\gradle-6.5-bin\caches\transforms-3\9f8ebb556426a803d22b2923460835c8\transformed\jetified-facebook-core-5.15.3\AndroidManifest.xml:46:13-37
216            <intent-filter>
216-->[com.facebook.android:facebook-core:5.15.3] D:\Downloads\gradle-6.5-bin\caches\transforms-3\9f8ebb556426a803d22b2923460835c8\transformed\jetified-facebook-core-5.15.3\AndroidManifest.xml:47:13-49:29
217                <action android:name="com.facebook.sdk.ACTION_CURRENT_ACCESS_TOKEN_CHANGED" />
217-->[com.facebook.android:facebook-core:5.15.3] D:\Downloads\gradle-6.5-bin\caches\transforms-3\9f8ebb556426a803d22b2923460835c8\transformed\jetified-facebook-core-5.15.3\AndroidManifest.xml:48:17-95
217-->[com.facebook.android:facebook-core:5.15.3] D:\Downloads\gradle-6.5-bin\caches\transforms-3\9f8ebb556426a803d22b2923460835c8\transformed\jetified-facebook-core-5.15.3\AndroidManifest.xml:48:25-92
218            </intent-filter>
219        </receiver>
220    </application>
221
222</manifest>
