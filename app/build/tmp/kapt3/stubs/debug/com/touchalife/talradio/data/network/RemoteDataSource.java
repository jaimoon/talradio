package com.touchalife.talradio.data.network;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\b\u0007\u00a2\u0006\u0002\u0010\u0002J\'\u0010\u0003\u001a\u0002H\u0004\"\u0004\b\u0000\u0010\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u0002H\u00040\u00062\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tJ\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u001c\u0010\f\u001a\u00020\r2\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0007\u001a\u00020\bH\u0002\u00a8\u0006\u0010"}, d2 = {"Lcom/touchalife/talradio/data/network/RemoteDataSource;", "", "()V", "buildApi", "Api", "api", "Ljava/lang/Class;", "context", "Landroid/content/Context;", "(Ljava/lang/Class;Landroid/content/Context;)Ljava/lang/Object;", "buildTokenApi", "Lcom/touchalife/talradio/data/network/Api;", "getRetrofitClient", "Lokhttp3/OkHttpClient;", "authenticator", "Lokhttp3/Authenticator;", "app_debug"})
public final class RemoteDataSource {
    
    @javax.inject.Inject()
    public RemoteDataSource() {
        super();
    }
    
    public final <Api extends java.lang.Object>Api buildApi(@org.jetbrains.annotations.NotNull()
    java.lang.Class<Api> api, @org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return null;
    }
    
    private final com.touchalife.talradio.data.network.Api buildTokenApi(android.content.Context context) {
        return null;
    }
    
    private final okhttp3.OkHttpClient getRetrofitClient(okhttp3.Authenticator authenticator, android.content.Context context) {
        return null;
    }
}