package com.touchalife.talradio.data.network;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0017\b\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u001c\u0010\f\u001a\u0004\u0018\u00010\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0017\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0015R\u0016\u0010\b\u001a\n \t*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0016"}, d2 = {"Lcom/touchalife/talradio/data/network/TokenAuthenticator;", "Lokhttp3/Authenticator;", "Lcom/touchalife/talradio/data/repository/BaseRepository;", "context", "Landroid/content/Context;", "tokenApi", "Lcom/touchalife/talradio/data/network/Api;", "(Landroid/content/Context;Lcom/touchalife/talradio/data/network/Api;)V", "appContext", "kotlin.jvm.PlatformType", "userPreferences", "Lcom/touchalife/talradio/data/UserPreferences;", "authenticate", "Lokhttp3/Request;", "route", "Lokhttp3/Route;", "response", "Lokhttp3/Response;", "getUpdatedToken", "Lcom/touchalife/talradio/data/network/Resource;", "Lcom/touchalife/talradio/data/responses/TokenResponse;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class TokenAuthenticator extends com.touchalife.talradio.data.repository.BaseRepository implements okhttp3.Authenticator {
    private final com.touchalife.talradio.data.network.Api tokenApi = null;
    private final android.content.Context appContext = null;
    private final com.touchalife.talradio.data.UserPreferences userPreferences = null;
    
    @javax.inject.Inject()
    public TokenAuthenticator(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.touchalife.talradio.data.network.Api tokenApi) {
        super(null);
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public okhttp3.Request authenticate(@org.jetbrains.annotations.Nullable()
    okhttp3.Route route, @org.jetbrains.annotations.NotNull()
    okhttp3.Response response) {
        return null;
    }
    
    private final java.lang.Object getUpdatedToken(kotlin.coroutines.Continuation<? super com.touchalife.talradio.data.network.Resource<com.touchalife.talradio.data.responses.TokenResponse>> continuation) {
        return null;
    }
}