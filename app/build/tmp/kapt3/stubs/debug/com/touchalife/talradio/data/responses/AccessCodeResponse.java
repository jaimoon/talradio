package com.touchalife.talradio.data.responses;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u0018B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00072\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\t\u0010\u0017\u001a\u00020\u0005H\u00d6\u0001R\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0016\u0010\u0004\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0016\u0010\u0006\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u0019"}, d2 = {"Lcom/touchalife/talradio/data/responses/AccessCodeResponse;", "", "data", "Lcom/touchalife/talradio/data/responses/AccessCodeResponse$Data;", "message", "", "success", "", "(Lcom/touchalife/talradio/data/responses/AccessCodeResponse$Data;Ljava/lang/String;Z)V", "getData", "()Lcom/touchalife/talradio/data/responses/AccessCodeResponse$Data;", "getMessage", "()Ljava/lang/String;", "getSuccess", "()Z", "component1", "component2", "component3", "copy", "equals", "other", "hashCode", "", "toString", "Data", "app_debug"})
public final class AccessCodeResponse {
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "data")
    private final com.touchalife.talradio.data.responses.AccessCodeResponse.Data data = null;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "message")
    private final java.lang.String message = null;
    @com.google.gson.annotations.SerializedName(value = "success")
    private final boolean success = false;
    
    @org.jetbrains.annotations.NotNull()
    public final com.touchalife.talradio.data.responses.AccessCodeResponse copy(@org.jetbrains.annotations.NotNull()
    com.touchalife.talradio.data.responses.AccessCodeResponse.Data data, @org.jetbrains.annotations.NotNull()
    java.lang.String message, boolean success) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public AccessCodeResponse(@org.jetbrains.annotations.NotNull()
    com.touchalife.talradio.data.responses.AccessCodeResponse.Data data, @org.jetbrains.annotations.NotNull()
    java.lang.String message, boolean success) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.touchalife.talradio.data.responses.AccessCodeResponse.Data component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.touchalife.talradio.data.responses.AccessCodeResponse.Data getData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMessage() {
        return null;
    }
    
    public final boolean component3() {
        return false;
    }
    
    public final boolean getSuccess() {
        return false;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u0001!B5\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u0012\u0006\u0010\u0003\u001a\u00020\u0001\u0012\u0006\u0010\u0004\u001a\u00020\u0001\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0014\u001a\u00020\u0001H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0001H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0001H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\tH\u00c6\u0003JE\u0010\u001a\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00012\b\b\u0002\u0010\u0003\u001a\u00020\u00012\b\b\u0002\u0010\u0004\u001a\u00020\u00012\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010\b\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u001b\u001a\u00020\u001c2\b\u0010\u001d\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001e\u001a\u00020\u001fH\u00d6\u0001J\t\u0010 \u001a\u00020\u0006H\u00d6\u0001R\u0016\u0010\u0002\u001a\u00020\u00018\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0016\u0010\u0003\u001a\u00020\u00018\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\fR\u0016\u0010\u0004\u001a\u00020\u00018\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\fR\u0016\u0010\u0005\u001a\u00020\u00068\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0007\u001a\u00020\u00068\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0010R\u0016\u0010\b\u001a\u00020\t8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013\u00a8\u0006\""}, d2 = {"Lcom/touchalife/talradio/data/responses/AccessCodeResponse$Data;", "", "greeting", "hugPic", "profilePic", "refreshToken", "", "token", "user", "Lcom/touchalife/talradio/data/responses/AccessCodeResponse$Data$User;", "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Lcom/touchalife/talradio/data/responses/AccessCodeResponse$Data$User;)V", "getGreeting", "()Ljava/lang/Object;", "getHugPic", "getProfilePic", "getRefreshToken", "()Ljava/lang/String;", "getToken", "getUser", "()Lcom/touchalife/talradio/data/responses/AccessCodeResponse$Data$User;", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "", "other", "hashCode", "", "toString", "User", "app_debug"})
    public static final class Data {
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "greeting")
        private final java.lang.Object greeting = null;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "hugPic")
        private final java.lang.Object hugPic = null;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "profilePic")
        private final java.lang.Object profilePic = null;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "refreshToken")
        private final java.lang.String refreshToken = null;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "token")
        private final java.lang.String token = null;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "user")
        private final com.touchalife.talradio.data.responses.AccessCodeResponse.Data.User user = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.touchalife.talradio.data.responses.AccessCodeResponse.Data copy(@org.jetbrains.annotations.NotNull()
        java.lang.Object greeting, @org.jetbrains.annotations.NotNull()
        java.lang.Object hugPic, @org.jetbrains.annotations.NotNull()
        java.lang.Object profilePic, @org.jetbrains.annotations.NotNull()
        java.lang.String refreshToken, @org.jetbrains.annotations.NotNull()
        java.lang.String token, @org.jetbrains.annotations.NotNull()
        com.touchalife.talradio.data.responses.AccessCodeResponse.Data.User user) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public Data(@org.jetbrains.annotations.NotNull()
        java.lang.Object greeting, @org.jetbrains.annotations.NotNull()
        java.lang.Object hugPic, @org.jetbrains.annotations.NotNull()
        java.lang.Object profilePic, @org.jetbrains.annotations.NotNull()
        java.lang.String refreshToken, @org.jetbrains.annotations.NotNull()
        java.lang.String token, @org.jetbrains.annotations.NotNull()
        com.touchalife.talradio.data.responses.AccessCodeResponse.Data.User user) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.Object component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.Object getGreeting() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.Object component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.Object getHugPic() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.Object component3() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.Object getProfilePic() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component4() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getRefreshToken() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component5() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getToken() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.touchalife.talradio.data.responses.AccessCodeResponse.Data.User component6() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.touchalife.talradio.data.responses.AccessCodeResponse.Data.User getUser() {
            return null;
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\r\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u0012\u0006\u0010\u0003\u001a\u00020\u0001\u0012\u0006\u0010\u0004\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u0005J\t\u0010\n\u001a\u00020\u0001H\u00c6\u0003J\t\u0010\u000b\u001a\u00020\u0001H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0001H\u00c6\u0003J\'\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00012\b\b\u0002\u0010\u0003\u001a\u00020\u00012\b\b\u0002\u0010\u0004\u001a\u00020\u0001H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001R\u0016\u0010\u0002\u001a\u00020\u00018\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00018\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007R\u0016\u0010\u0004\u001a\u00020\u00018\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\u0007\u00a8\u0006\u0015"}, d2 = {"Lcom/touchalife/talradio/data/responses/AccessCodeResponse$Data$User;", "", "avatar", "gender", "name", "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V", "getAvatar", "()Ljava/lang/Object;", "getGender", "getName", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_debug"})
        public static final class User {
            @org.jetbrains.annotations.NotNull()
            @com.google.gson.annotations.SerializedName(value = "avatar")
            private final java.lang.Object avatar = null;
            @org.jetbrains.annotations.NotNull()
            @com.google.gson.annotations.SerializedName(value = "gender")
            private final java.lang.Object gender = null;
            @org.jetbrains.annotations.NotNull()
            @com.google.gson.annotations.SerializedName(value = "name")
            private final java.lang.Object name = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.touchalife.talradio.data.responses.AccessCodeResponse.Data.User copy(@org.jetbrains.annotations.NotNull()
            java.lang.Object avatar, @org.jetbrains.annotations.NotNull()
            java.lang.Object gender, @org.jetbrains.annotations.NotNull()
            java.lang.Object name) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public User(@org.jetbrains.annotations.NotNull()
            java.lang.Object avatar, @org.jetbrains.annotations.NotNull()
            java.lang.Object gender, @org.jetbrains.annotations.NotNull()
            java.lang.Object name) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.Object component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.Object getAvatar() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.Object component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.Object getGender() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.Object component3() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.Object getName() {
                return null;
            }
        }
    }
}