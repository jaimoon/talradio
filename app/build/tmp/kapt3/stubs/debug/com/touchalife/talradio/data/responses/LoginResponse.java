package com.touchalife.talradio.data.responses;

import java.lang.System;

@androidx.annotation.Keep()
@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0002\b\u0005\b\u0087\b\u0018\u00002\u00020\u0001:\u0001\u001bB%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\bH\u00c6\u0003J1\u0010\u0015\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\bH\u00c6\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\bH\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u0005H\u00d6\u0001R\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0016\u0010\u0004\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0016\u0010\u0006\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0016\u0010\u0007\u001a\u00020\b8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010\u00a8\u0006\u001c"}, d2 = {"Lcom/touchalife/talradio/data/responses/LoginResponse;", "", "data", "Lcom/touchalife/talradio/data/responses/LoginResponse$Data;", "message", "", "status", "statusCode", "", "(Lcom/touchalife/talradio/data/responses/LoginResponse$Data;Ljava/lang/String;Ljava/lang/String;I)V", "getData", "()Lcom/touchalife/talradio/data/responses/LoginResponse$Data;", "getMessage", "()Ljava/lang/String;", "getStatus", "getStatusCode", "()I", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "toString", "Data", "app_debug"})
public final class LoginResponse {
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "data")
    private final com.touchalife.talradio.data.responses.LoginResponse.Data data = null;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "message")
    private final java.lang.String message = null;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "status")
    private final java.lang.String status = null;
    @com.google.gson.annotations.SerializedName(value = "statusCode")
    private final int statusCode = 0;
    
    @org.jetbrains.annotations.NotNull()
    public final com.touchalife.talradio.data.responses.LoginResponse copy(@org.jetbrains.annotations.NotNull()
    com.touchalife.talradio.data.responses.LoginResponse.Data data, @org.jetbrains.annotations.NotNull()
    java.lang.String message, @org.jetbrains.annotations.NotNull()
    java.lang.String status, int statusCode) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public LoginResponse(@org.jetbrains.annotations.NotNull()
    com.touchalife.talradio.data.responses.LoginResponse.Data data, @org.jetbrains.annotations.NotNull()
    java.lang.String message, @org.jetbrains.annotations.NotNull()
    java.lang.String status, int statusCode) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.touchalife.talradio.data.responses.LoginResponse.Data component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.touchalife.talradio.data.responses.LoginResponse.Data getData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMessage() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getStatus() {
        return null;
    }
    
    public final int component4() {
        return 0;
    }
    
    public final int getStatusCode() {
        return 0;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000e\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\bW\b\u0086\b\u0018\u00002\u00020\u0001:\u0005{|}~\u007fB\u00a1\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\u0003\u0012\u0006\u0010\r\u001a\u00020\u000b\u0012\u0006\u0010\u000e\u001a\u00020\u0005\u0012\u0006\u0010\u000f\u001a\u00020\u000b\u0012\u0006\u0010\u0010\u001a\u00020\u000b\u0012\u0006\u0010\u0011\u001a\u00020\u000b\u0012\u0006\u0010\u0012\u001a\u00020\u000b\u0012\u0006\u0010\u0013\u001a\u00020\u0005\u0012\u0006\u0010\u0014\u001a\u00020\u0005\u0012\u0006\u0010\u0015\u001a\u00020\u0005\u0012\u0006\u0010\u0016\u001a\u00020\u0005\u0012\u0006\u0010\u0017\u001a\u00020\u0005\u0012\u0006\u0010\u0018\u001a\u00020\u0003\u0012\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00010\u001a\u0012\u0006\u0010\u001b\u001a\u00020\u000b\u0012\u0006\u0010\u001c\u001a\u00020\u001d\u0012\u0006\u0010\u001e\u001a\u00020\u0005\u0012\u0006\u0010\u001f\u001a\u00020\u000b\u0012\u0006\u0010 \u001a\u00020\u0005\u0012\u0006\u0010!\u001a\u00020\u000b\u0012\u0006\u0010\"\u001a\u00020\u0003\u0012\f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u000b0\u001a\u0012\u0006\u0010$\u001a\u00020%\u0012\u0006\u0010&\u001a\u00020\u000b\u0012\u0006\u0010\'\u001a\u00020\u000b\u0012\u0006\u0010(\u001a\u00020)\u0012\u0006\u0010*\u001a\u00020\u0003\u0012\u0006\u0010+\u001a\u00020\u000b\u0012\u0006\u0010,\u001a\u00020\u000b\u00a2\u0006\u0002\u0010-J\t\u0010T\u001a\u00020\u0003H\u00c6\u0003J\t\u0010U\u001a\u00020\u000bH\u00c6\u0003J\t\u0010V\u001a\u00020\u000bH\u00c6\u0003J\t\u0010W\u001a\u00020\u000bH\u00c6\u0003J\t\u0010X\u001a\u00020\u0005H\u00c6\u0003J\t\u0010Y\u001a\u00020\u0005H\u00c6\u0003J\t\u0010Z\u001a\u00020\u0005H\u00c6\u0003J\t\u0010[\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\\\u001a\u00020\u0005H\u00c6\u0003J\t\u0010]\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010^\u001a\b\u0012\u0004\u0012\u00020\u00010\u001aH\u00c6\u0003J\t\u0010_\u001a\u00020\u0005H\u00c6\u0003J\t\u0010`\u001a\u00020\u000bH\u00c6\u0003J\t\u0010a\u001a\u00020\u001dH\u00c6\u0003J\t\u0010b\u001a\u00020\u0005H\u00c6\u0003J\t\u0010c\u001a\u00020\u000bH\u00c6\u0003J\t\u0010d\u001a\u00020\u0005H\u00c6\u0003J\t\u0010e\u001a\u00020\u000bH\u00c6\u0003J\t\u0010f\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010g\u001a\b\u0012\u0004\u0012\u00020\u000b0\u001aH\u00c6\u0003J\t\u0010h\u001a\u00020%H\u00c6\u0003J\t\u0010i\u001a\u00020\u000bH\u00c6\u0003J\t\u0010j\u001a\u00020\u0007H\u00c6\u0003J\t\u0010k\u001a\u00020\u000bH\u00c6\u0003J\t\u0010l\u001a\u00020)H\u00c6\u0003J\t\u0010m\u001a\u00020\u0003H\u00c6\u0003J\t\u0010n\u001a\u00020\u000bH\u00c6\u0003J\t\u0010o\u001a\u00020\u000bH\u00c6\u0003J\t\u0010p\u001a\u00020\tH\u00c6\u0003J\t\u0010q\u001a\u00020\u000bH\u00c6\u0003J\t\u0010r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010s\u001a\u00020\u000bH\u00c6\u0003J\t\u0010t\u001a\u00020\u0005H\u00c6\u0003J\t\u0010u\u001a\u00020\u000bH\u00c6\u0003J\u00e9\u0002\u0010v\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\u00032\b\b\u0002\u0010\r\u001a\u00020\u000b2\b\b\u0002\u0010\u000e\u001a\u00020\u00052\b\b\u0002\u0010\u000f\u001a\u00020\u000b2\b\b\u0002\u0010\u0010\u001a\u00020\u000b2\b\b\u0002\u0010\u0011\u001a\u00020\u000b2\b\b\u0002\u0010\u0012\u001a\u00020\u000b2\b\b\u0002\u0010\u0013\u001a\u00020\u00052\b\b\u0002\u0010\u0014\u001a\u00020\u00052\b\b\u0002\u0010\u0015\u001a\u00020\u00052\b\b\u0002\u0010\u0016\u001a\u00020\u00052\b\b\u0002\u0010\u0017\u001a\u00020\u00052\b\b\u0002\u0010\u0018\u001a\u00020\u00032\u000e\b\u0002\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00010\u001a2\b\b\u0002\u0010\u001b\u001a\u00020\u000b2\b\b\u0002\u0010\u001c\u001a\u00020\u001d2\b\b\u0002\u0010\u001e\u001a\u00020\u00052\b\b\u0002\u0010\u001f\u001a\u00020\u000b2\b\b\u0002\u0010 \u001a\u00020\u00052\b\b\u0002\u0010!\u001a\u00020\u000b2\b\b\u0002\u0010\"\u001a\u00020\u00032\u000e\b\u0002\u0010#\u001a\b\u0012\u0004\u0012\u00020\u000b0\u001a2\b\b\u0002\u0010$\u001a\u00020%2\b\b\u0002\u0010&\u001a\u00020\u000b2\b\b\u0002\u0010\'\u001a\u00020\u000b2\b\b\u0002\u0010(\u001a\u00020)2\b\b\u0002\u0010*\u001a\u00020\u00032\b\b\u0002\u0010+\u001a\u00020\u000b2\b\b\u0002\u0010,\u001a\u00020\u000bH\u00c6\u0001J\u0013\u0010w\u001a\u00020\u00052\b\u0010x\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010y\u001a\u00020\u0003H\u00d6\u0001J\t\u0010z\u001a\u00020\u000bH\u00d6\u0001R\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b.\u0010/R\u0016\u0010\u0004\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b0\u00101R\u0016\u0010\u0006\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b2\u00103R\u0016\u0010\b\u001a\u00020\t8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b4\u00105R\u0016\u0010\n\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b6\u00107R\u0016\u0010\f\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b8\u0010/R\u0016\u0010\r\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b9\u00107R\u0016\u0010\u000e\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b:\u00101R\u0016\u0010\u000f\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b;\u00107R\u0016\u0010\u0010\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b<\u00107R\u0016\u0010\u0011\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b=\u00107R\u0016\u0010\u0012\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b>\u00107R\u0016\u0010\u0013\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u00101R\u0016\u0010\u0014\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u00101R\u0016\u0010\u0015\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u00101R\u0016\u0010\u0016\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u00101R\u0016\u0010\u0017\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u00101R\u0016\u0010\u0018\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b?\u0010/R\u001c\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00010\u001a8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b@\u0010AR\u0016\u0010\u001b\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\bB\u00107R\u0016\u0010\u001c\u001a\u00020\u001d8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\bC\u0010DR\u0016\u0010\u001e\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\bE\u00101R\u0016\u0010\u001f\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\bF\u00107R\u0016\u0010 \u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\bG\u00101R\u0016\u0010!\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\bH\u00107R\u0016\u0010\"\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\bI\u0010/R\u001c\u0010#\u001a\b\u0012\u0004\u0012\u00020\u000b0\u001a8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\bJ\u0010AR\u0016\u0010$\u001a\u00020%8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\bK\u0010LR\u0016\u0010&\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\bM\u00107R\u0016\u0010\'\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\bN\u00107R\u0016\u0010(\u001a\u00020)8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\bO\u0010PR\u0016\u0010*\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\bQ\u0010/R\u0016\u0010+\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\bR\u00107R\u0016\u0010,\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\bS\u00107\u00a8\u0006\u0080\u0001"}, d2 = {"Lcom/touchalife/talradio/data/responses/LoginResponse$Data;", "", "accountStatus", "", "accountVerified", "", "address", "Lcom/touchalife/talradio/data/responses/LoginResponse$Data$Address;", "billingAddress", "Lcom/touchalife/talradio/data/responses/LoginResponse$Data$BillingAddress;", "displayName", "", "dob", "email", "emailVerified", "gender", "institutionName", "institutionUrl", "institutionalRole", "isHackathonRegistered", "isKindnessEventRegistered", "isMentor", "isProfileCompleted", "isTourCompleted", "kindnessScore", "languages", "", "loginProvider", "name", "Lcom/touchalife/talradio/data/responses/LoginResponse$Data$Name;", "passwordVerified", "phone", "phoneVerified", "profileImageUrl", "rating", "roles", "socialVerification", "Lcom/touchalife/talradio/data/responses/LoginResponse$Data$SocialVerification;", "stripeCustomerId", "summary", "tokenDetail", "Lcom/touchalife/talradio/data/responses/LoginResponse$Data$TokenDetail;", "totalEmailsPerMonth", "uniqueId", "username", "(IZLcom/touchalife/talradio/data/responses/LoginResponse$Data$Address;Lcom/touchalife/talradio/data/responses/LoginResponse$Data$BillingAddress;Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZILjava/util/List;Ljava/lang/String;Lcom/touchalife/talradio/data/responses/LoginResponse$Data$Name;ZLjava/lang/String;ZLjava/lang/String;ILjava/util/List;Lcom/touchalife/talradio/data/responses/LoginResponse$Data$SocialVerification;Ljava/lang/String;Ljava/lang/String;Lcom/touchalife/talradio/data/responses/LoginResponse$Data$TokenDetail;ILjava/lang/String;Ljava/lang/String;)V", "getAccountStatus", "()I", "getAccountVerified", "()Z", "getAddress", "()Lcom/touchalife/talradio/data/responses/LoginResponse$Data$Address;", "getBillingAddress", "()Lcom/touchalife/talradio/data/responses/LoginResponse$Data$BillingAddress;", "getDisplayName", "()Ljava/lang/String;", "getDob", "getEmail", "getEmailVerified", "getGender", "getInstitutionName", "getInstitutionUrl", "getInstitutionalRole", "getKindnessScore", "getLanguages", "()Ljava/util/List;", "getLoginProvider", "getName", "()Lcom/touchalife/talradio/data/responses/LoginResponse$Data$Name;", "getPasswordVerified", "getPhone", "getPhoneVerified", "getProfileImageUrl", "getRating", "getRoles", "getSocialVerification", "()Lcom/touchalife/talradio/data/responses/LoginResponse$Data$SocialVerification;", "getStripeCustomerId", "getSummary", "getTokenDetail", "()Lcom/touchalife/talradio/data/responses/LoginResponse$Data$TokenDetail;", "getTotalEmailsPerMonth", "getUniqueId", "getUsername", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component19", "component2", "component20", "component21", "component22", "component23", "component24", "component25", "component26", "component27", "component28", "component29", "component3", "component30", "component31", "component32", "component33", "component34", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "toString", "Address", "BillingAddress", "Name", "SocialVerification", "TokenDetail", "app_debug"})
    public static final class Data {
        @com.google.gson.annotations.SerializedName(value = "account_status")
        private final int accountStatus = 0;
        @com.google.gson.annotations.SerializedName(value = "account_verified")
        private final boolean accountVerified = false;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "address")
        private final com.touchalife.talradio.data.responses.LoginResponse.Data.Address address = null;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "billingAddress")
        private final com.touchalife.talradio.data.responses.LoginResponse.Data.BillingAddress billingAddress = null;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "display_name")
        private final java.lang.String displayName = null;
        @com.google.gson.annotations.SerializedName(value = "dob")
        private final int dob = 0;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "email")
        private final java.lang.String email = null;
        @com.google.gson.annotations.SerializedName(value = "email_verified")
        private final boolean emailVerified = false;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "gender")
        private final java.lang.String gender = null;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "institutionName")
        private final java.lang.String institutionName = null;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "institutionUrl")
        private final java.lang.String institutionUrl = null;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "institutionalRole")
        private final java.lang.String institutionalRole = null;
        @com.google.gson.annotations.SerializedName(value = "isHackathonRegistered")
        private final boolean isHackathonRegistered = false;
        @com.google.gson.annotations.SerializedName(value = "isKindnessEventRegistered")
        private final boolean isKindnessEventRegistered = false;
        @com.google.gson.annotations.SerializedName(value = "isMentor")
        private final boolean isMentor = false;
        @com.google.gson.annotations.SerializedName(value = "isProfileCompleted")
        private final boolean isProfileCompleted = false;
        @com.google.gson.annotations.SerializedName(value = "isTourCompleted")
        private final boolean isTourCompleted = false;
        @com.google.gson.annotations.SerializedName(value = "kindness_score")
        private final int kindnessScore = 0;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "languages")
        private final java.util.List<java.lang.Object> languages = null;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "login_provider")
        private final java.lang.String loginProvider = null;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "name")
        private final com.touchalife.talradio.data.responses.LoginResponse.Data.Name name = null;
        @com.google.gson.annotations.SerializedName(value = "password_verified")
        private final boolean passwordVerified = false;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "phone")
        private final java.lang.String phone = null;
        @com.google.gson.annotations.SerializedName(value = "phone_verified")
        private final boolean phoneVerified = false;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "profile_image_url")
        private final java.lang.String profileImageUrl = null;
        @com.google.gson.annotations.SerializedName(value = "rating")
        private final int rating = 0;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "roles")
        private final java.util.List<java.lang.String> roles = null;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "social_verification")
        private final com.touchalife.talradio.data.responses.LoginResponse.Data.SocialVerification socialVerification = null;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "stripeCustomerId")
        private final java.lang.String stripeCustomerId = null;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "summary")
        private final java.lang.String summary = null;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "token_detail")
        private final com.touchalife.talradio.data.responses.LoginResponse.Data.TokenDetail tokenDetail = null;
        @com.google.gson.annotations.SerializedName(value = "totalEmailsPerMonth")
        private final int totalEmailsPerMonth = 0;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "unique_id")
        private final java.lang.String uniqueId = null;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "username")
        private final java.lang.String username = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.touchalife.talradio.data.responses.LoginResponse.Data copy(int accountStatus, boolean accountVerified, @org.jetbrains.annotations.NotNull()
        com.touchalife.talradio.data.responses.LoginResponse.Data.Address address, @org.jetbrains.annotations.NotNull()
        com.touchalife.talradio.data.responses.LoginResponse.Data.BillingAddress billingAddress, @org.jetbrains.annotations.NotNull()
        java.lang.String displayName, int dob, @org.jetbrains.annotations.NotNull()
        java.lang.String email, boolean emailVerified, @org.jetbrains.annotations.NotNull()
        java.lang.String gender, @org.jetbrains.annotations.NotNull()
        java.lang.String institutionName, @org.jetbrains.annotations.NotNull()
        java.lang.String institutionUrl, @org.jetbrains.annotations.NotNull()
        java.lang.String institutionalRole, boolean isHackathonRegistered, boolean isKindnessEventRegistered, boolean isMentor, boolean isProfileCompleted, boolean isTourCompleted, int kindnessScore, @org.jetbrains.annotations.NotNull()
        java.util.List<? extends java.lang.Object> languages, @org.jetbrains.annotations.NotNull()
        java.lang.String loginProvider, @org.jetbrains.annotations.NotNull()
        com.touchalife.talradio.data.responses.LoginResponse.Data.Name name, boolean passwordVerified, @org.jetbrains.annotations.NotNull()
        java.lang.String phone, boolean phoneVerified, @org.jetbrains.annotations.NotNull()
        java.lang.String profileImageUrl, int rating, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> roles, @org.jetbrains.annotations.NotNull()
        com.touchalife.talradio.data.responses.LoginResponse.Data.SocialVerification socialVerification, @org.jetbrains.annotations.NotNull()
        java.lang.String stripeCustomerId, @org.jetbrains.annotations.NotNull()
        java.lang.String summary, @org.jetbrains.annotations.NotNull()
        com.touchalife.talradio.data.responses.LoginResponse.Data.TokenDetail tokenDetail, int totalEmailsPerMonth, @org.jetbrains.annotations.NotNull()
        java.lang.String uniqueId, @org.jetbrains.annotations.NotNull()
        java.lang.String username) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public Data(int accountStatus, boolean accountVerified, @org.jetbrains.annotations.NotNull()
        com.touchalife.talradio.data.responses.LoginResponse.Data.Address address, @org.jetbrains.annotations.NotNull()
        com.touchalife.talradio.data.responses.LoginResponse.Data.BillingAddress billingAddress, @org.jetbrains.annotations.NotNull()
        java.lang.String displayName, int dob, @org.jetbrains.annotations.NotNull()
        java.lang.String email, boolean emailVerified, @org.jetbrains.annotations.NotNull()
        java.lang.String gender, @org.jetbrains.annotations.NotNull()
        java.lang.String institutionName, @org.jetbrains.annotations.NotNull()
        java.lang.String institutionUrl, @org.jetbrains.annotations.NotNull()
        java.lang.String institutionalRole, boolean isHackathonRegistered, boolean isKindnessEventRegistered, boolean isMentor, boolean isProfileCompleted, boolean isTourCompleted, int kindnessScore, @org.jetbrains.annotations.NotNull()
        java.util.List<? extends java.lang.Object> languages, @org.jetbrains.annotations.NotNull()
        java.lang.String loginProvider, @org.jetbrains.annotations.NotNull()
        com.touchalife.talradio.data.responses.LoginResponse.Data.Name name, boolean passwordVerified, @org.jetbrains.annotations.NotNull()
        java.lang.String phone, boolean phoneVerified, @org.jetbrains.annotations.NotNull()
        java.lang.String profileImageUrl, int rating, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> roles, @org.jetbrains.annotations.NotNull()
        com.touchalife.talradio.data.responses.LoginResponse.Data.SocialVerification socialVerification, @org.jetbrains.annotations.NotNull()
        java.lang.String stripeCustomerId, @org.jetbrains.annotations.NotNull()
        java.lang.String summary, @org.jetbrains.annotations.NotNull()
        com.touchalife.talradio.data.responses.LoginResponse.Data.TokenDetail tokenDetail, int totalEmailsPerMonth, @org.jetbrains.annotations.NotNull()
        java.lang.String uniqueId, @org.jetbrains.annotations.NotNull()
        java.lang.String username) {
            super();
        }
        
        public final int component1() {
            return 0;
        }
        
        public final int getAccountStatus() {
            return 0;
        }
        
        public final boolean component2() {
            return false;
        }
        
        public final boolean getAccountVerified() {
            return false;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.touchalife.talradio.data.responses.LoginResponse.Data.Address component3() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.touchalife.talradio.data.responses.LoginResponse.Data.Address getAddress() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.touchalife.talradio.data.responses.LoginResponse.Data.BillingAddress component4() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.touchalife.talradio.data.responses.LoginResponse.Data.BillingAddress getBillingAddress() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component5() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getDisplayName() {
            return null;
        }
        
        public final int component6() {
            return 0;
        }
        
        public final int getDob() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component7() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getEmail() {
            return null;
        }
        
        public final boolean component8() {
            return false;
        }
        
        public final boolean getEmailVerified() {
            return false;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component9() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getGender() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component10() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getInstitutionName() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component11() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getInstitutionUrl() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component12() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getInstitutionalRole() {
            return null;
        }
        
        public final boolean component13() {
            return false;
        }
        
        public final boolean isHackathonRegistered() {
            return false;
        }
        
        public final boolean component14() {
            return false;
        }
        
        public final boolean isKindnessEventRegistered() {
            return false;
        }
        
        public final boolean component15() {
            return false;
        }
        
        public final boolean isMentor() {
            return false;
        }
        
        public final boolean component16() {
            return false;
        }
        
        public final boolean isProfileCompleted() {
            return false;
        }
        
        public final boolean component17() {
            return false;
        }
        
        public final boolean isTourCompleted() {
            return false;
        }
        
        public final int component18() {
            return 0;
        }
        
        public final int getKindnessScore() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.Object> component19() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.Object> getLanguages() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component20() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getLoginProvider() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.touchalife.talradio.data.responses.LoginResponse.Data.Name component21() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.touchalife.talradio.data.responses.LoginResponse.Data.Name getName() {
            return null;
        }
        
        public final boolean component22() {
            return false;
        }
        
        public final boolean getPasswordVerified() {
            return false;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component23() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getPhone() {
            return null;
        }
        
        public final boolean component24() {
            return false;
        }
        
        public final boolean getPhoneVerified() {
            return false;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component25() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getProfileImageUrl() {
            return null;
        }
        
        public final int component26() {
            return 0;
        }
        
        public final int getRating() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> component27() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> getRoles() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.touchalife.talradio.data.responses.LoginResponse.Data.SocialVerification component28() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.touchalife.talradio.data.responses.LoginResponse.Data.SocialVerification getSocialVerification() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component29() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getStripeCustomerId() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component30() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getSummary() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.touchalife.talradio.data.responses.LoginResponse.Data.TokenDetail component31() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.touchalife.talradio.data.responses.LoginResponse.Data.TokenDetail getTokenDetail() {
            return null;
        }
        
        public final int component32() {
            return 0;
        }
        
        public final int getTotalEmailsPerMonth() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component33() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getUniqueId() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component34() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getUsername() {
            return null;
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0012\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J;\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u0003H\u00d6\u0001R\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0016\u0010\u0004\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\nR\u0016\u0010\u0005\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\nR\u0016\u0010\u0006\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\nR\u0016\u0010\u0007\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\n\u00a8\u0006\u001b"}, d2 = {"Lcom/touchalife/talradio/data/responses/LoginResponse$Data$Address;", "", "city", "", "country", "line1", "line2", "state", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getCity", "()Ljava/lang/String;", "getCountry", "getLine1", "getLine2", "getState", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "", "other", "hashCode", "", "toString", "app_debug"})
        public static final class Address {
            @org.jetbrains.annotations.NotNull()
            @com.google.gson.annotations.SerializedName(value = "city")
            private final java.lang.String city = null;
            @org.jetbrains.annotations.NotNull()
            @com.google.gson.annotations.SerializedName(value = "country")
            private final java.lang.String country = null;
            @org.jetbrains.annotations.NotNull()
            @com.google.gson.annotations.SerializedName(value = "line1")
            private final java.lang.String line1 = null;
            @org.jetbrains.annotations.NotNull()
            @com.google.gson.annotations.SerializedName(value = "line2")
            private final java.lang.String line2 = null;
            @org.jetbrains.annotations.NotNull()
            @com.google.gson.annotations.SerializedName(value = "state")
            private final java.lang.String state = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.touchalife.talradio.data.responses.LoginResponse.Data.Address copy(@org.jetbrains.annotations.NotNull()
            java.lang.String city, @org.jetbrains.annotations.NotNull()
            java.lang.String country, @org.jetbrains.annotations.NotNull()
            java.lang.String line1, @org.jetbrains.annotations.NotNull()
            java.lang.String line2, @org.jetbrains.annotations.NotNull()
            java.lang.String state) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Address(@org.jetbrains.annotations.NotNull()
            java.lang.String city, @org.jetbrains.annotations.NotNull()
            java.lang.String country, @org.jetbrains.annotations.NotNull()
            java.lang.String line1, @org.jetbrains.annotations.NotNull()
            java.lang.String line2, @org.jetbrains.annotations.NotNull()
            java.lang.String state) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getCity() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getCountry() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component3() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getLine1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component4() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getLine2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component5() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getState() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0012\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J;\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u0003H\u00d6\u0001R\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0016\u0010\u0004\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\nR\u0016\u0010\u0005\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\nR\u0016\u0010\u0006\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\nR\u0016\u0010\u0007\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\n\u00a8\u0006\u001b"}, d2 = {"Lcom/touchalife/talradio/data/responses/LoginResponse$Data$BillingAddress;", "", "city", "", "country", "line1", "line2", "state", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getCity", "()Ljava/lang/String;", "getCountry", "getLine1", "getLine2", "getState", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "", "other", "hashCode", "", "toString", "app_debug"})
        public static final class BillingAddress {
            @org.jetbrains.annotations.NotNull()
            @com.google.gson.annotations.SerializedName(value = "city")
            private final java.lang.String city = null;
            @org.jetbrains.annotations.NotNull()
            @com.google.gson.annotations.SerializedName(value = "country")
            private final java.lang.String country = null;
            @org.jetbrains.annotations.NotNull()
            @com.google.gson.annotations.SerializedName(value = "line1")
            private final java.lang.String line1 = null;
            @org.jetbrains.annotations.NotNull()
            @com.google.gson.annotations.SerializedName(value = "line2")
            private final java.lang.String line2 = null;
            @org.jetbrains.annotations.NotNull()
            @com.google.gson.annotations.SerializedName(value = "state")
            private final java.lang.String state = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.touchalife.talradio.data.responses.LoginResponse.Data.BillingAddress copy(@org.jetbrains.annotations.NotNull()
            java.lang.String city, @org.jetbrains.annotations.NotNull()
            java.lang.String country, @org.jetbrains.annotations.NotNull()
            java.lang.String line1, @org.jetbrains.annotations.NotNull()
            java.lang.String line2, @org.jetbrains.annotations.NotNull()
            java.lang.String state) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public BillingAddress(@org.jetbrains.annotations.NotNull()
            java.lang.String city, @org.jetbrains.annotations.NotNull()
            java.lang.String country, @org.jetbrains.annotations.NotNull()
            java.lang.String line1, @org.jetbrains.annotations.NotNull()
            java.lang.String line2, @org.jetbrains.annotations.NotNull()
            java.lang.String state) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getCity() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getCountry() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component3() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getLine1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component4() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getLine2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component5() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getState() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\'\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0003H\u00d6\u0001R\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0016\u0010\u0004\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\bR\u0016\u0010\u0005\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\b\u00a8\u0006\u0015"}, d2 = {"Lcom/touchalife/talradio/data/responses/LoginResponse$Data$Name;", "", "firstName", "", "lastName", "middleName", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getFirstName", "()Ljava/lang/String;", "getLastName", "getMiddleName", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "app_debug"})
        public static final class Name {
            @org.jetbrains.annotations.NotNull()
            @com.google.gson.annotations.SerializedName(value = "first_name")
            private final java.lang.String firstName = null;
            @org.jetbrains.annotations.NotNull()
            @com.google.gson.annotations.SerializedName(value = "last_name")
            private final java.lang.String lastName = null;
            @org.jetbrains.annotations.NotNull()
            @com.google.gson.annotations.SerializedName(value = "middle_name")
            private final java.lang.String middleName = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.touchalife.talradio.data.responses.LoginResponse.Data.Name copy(@org.jetbrains.annotations.NotNull()
            java.lang.String firstName, @org.jetbrains.annotations.NotNull()
            java.lang.String lastName, @org.jetbrains.annotations.NotNull()
            java.lang.String middleName) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Name(@org.jetbrains.annotations.NotNull()
            java.lang.String firstName, @org.jetbrains.annotations.NotNull()
            java.lang.String lastName, @org.jetbrains.annotations.NotNull()
            java.lang.String middleName) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getFirstName() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getLastName() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component3() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getMiddleName() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0014\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J;\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u0015\u001a\u00020\u00032\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001R\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0016\u0010\u0004\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\nR\u0016\u0010\u0005\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\nR\u0016\u0010\u0006\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\nR\u0016\u0010\u0007\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\n\u00a8\u0006\u001b"}, d2 = {"Lcom/touchalife/talradio/data/responses/LoginResponse$Data$SocialVerification;", "", "appleVerified", "", "facebookVerified", "googleVerified", "linkedinVerified", "twitterVerified", "(ZZZZZ)V", "getAppleVerified", "()Z", "getFacebookVerified", "getGoogleVerified", "getLinkedinVerified", "getTwitterVerified", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "other", "hashCode", "", "toString", "", "app_debug"})
        public static final class SocialVerification {
            @com.google.gson.annotations.SerializedName(value = "apple_verified")
            private final boolean appleVerified = false;
            @com.google.gson.annotations.SerializedName(value = "facebook_verified")
            private final boolean facebookVerified = false;
            @com.google.gson.annotations.SerializedName(value = "google_verified")
            private final boolean googleVerified = false;
            @com.google.gson.annotations.SerializedName(value = "linkedin_verified")
            private final boolean linkedinVerified = false;
            @com.google.gson.annotations.SerializedName(value = "twitter_verified")
            private final boolean twitterVerified = false;
            
            @org.jetbrains.annotations.NotNull()
            public final com.touchalife.talradio.data.responses.LoginResponse.Data.SocialVerification copy(boolean appleVerified, boolean facebookVerified, boolean googleVerified, boolean linkedinVerified, boolean twitterVerified) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public SocialVerification(boolean appleVerified, boolean facebookVerified, boolean googleVerified, boolean linkedinVerified, boolean twitterVerified) {
                super();
            }
            
            public final boolean component1() {
                return false;
            }
            
            public final boolean getAppleVerified() {
                return false;
            }
            
            public final boolean component2() {
                return false;
            }
            
            public final boolean getFacebookVerified() {
                return false;
            }
            
            public final boolean component3() {
                return false;
            }
            
            public final boolean getGoogleVerified() {
                return false;
            }
            
            public final boolean component4() {
                return false;
            }
            
            public final boolean getLinkedinVerified() {
                return false;
            }
            
            public final boolean component5() {
                return false;
            }
            
            public final boolean getTwitterVerified() {
                return false;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\n\u001a\u00020\u0003H\u00c6\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001J\t\u0010\u0011\u001a\u00020\u0003H\u00d6\u0001R\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0004\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007\u00a8\u0006\u0012"}, d2 = {"Lcom/touchalife/talradio/data/responses/LoginResponse$Data$TokenDetail;", "", "token", "", "type", "(Ljava/lang/String;Ljava/lang/String;)V", "getToken", "()Ljava/lang/String;", "getType", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "app_debug"})
        public static final class TokenDetail {
            @org.jetbrains.annotations.NotNull()
            @com.google.gson.annotations.SerializedName(value = "token")
            private final java.lang.String token = null;
            @org.jetbrains.annotations.NotNull()
            @com.google.gson.annotations.SerializedName(value = "type")
            private final java.lang.String type = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.touchalife.talradio.data.responses.LoginResponse.Data.TokenDetail copy(@org.jetbrains.annotations.NotNull()
            java.lang.String token, @org.jetbrains.annotations.NotNull()
            java.lang.String type) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public TokenDetail(@org.jetbrains.annotations.NotNull()
            java.lang.String token, @org.jetbrains.annotations.NotNull()
            java.lang.String type) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getToken() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getType() {
                return null;
            }
        }
    }
}