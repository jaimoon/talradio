package com.touchalife.talradio.ui.viewmodel;

import java.lang.System;

@dagger.hilt.android.lifecycle.HiltViewModel()
@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\t\b\u0007\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0014J\u0019\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0014H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0019JB\u0010\u001a\u001a\u00020\u00122\b\u0010\u001b\u001a\u0004\u0018\u00010\u00142\b\u0010\u001c\u001a\u0004\u0018\u00010\u00142\b\u0010\u001d\u001a\u0004\u0018\u00010\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u00142\b\u0010\u001e\u001a\u0004\u0018\u00010\u00142\b\u0010\u001f\u001a\u0004\u0018\u00010\u0014R\u001d\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u001d\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\nR\u001a\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0010\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u00070\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006 "}, d2 = {"Lcom/touchalife/talradio/ui/viewmodel/AuthViewModel;", "Lcom/touchalife/talradio/ui/viewmodel/BaseViewModel;", "repository", "Lcom/touchalife/talradio/data/repository/AuthRepository;", "(Lcom/touchalife/talradio/data/repository/AuthRepository;)V", "_loginStateFlow", "Lkotlinx/coroutines/flow/StateFlow;", "Lcom/touchalife/talradio/data/network/Resource;", "Lcom/touchalife/talradio/data/responses/LoginResponse;", "get_loginStateFlow", "()Lkotlinx/coroutines/flow/StateFlow;", "_socialLoginStateFlow", "Lcom/touchalife/talradio/data/responses/SocialLoginResponse;", "get_socialLoginStateFlow", "loginStateFlow", "Lkotlinx/coroutines/flow/MutableStateFlow;", "socialLoginStateFlow", "login", "Lkotlinx/coroutines/Job;", "email", "", "password", "saveAccessTokens", "", "accessToken", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "socialLogin", "personId", "idType", "verifyType", "displayName", "provider", "app_debug"})
public final class AuthViewModel extends com.touchalife.talradio.ui.viewmodel.BaseViewModel {
    private final com.touchalife.talradio.data.repository.AuthRepository repository = null;
    private final kotlinx.coroutines.flow.MutableStateFlow<com.touchalife.talradio.data.network.Resource<com.touchalife.talradio.data.responses.LoginResponse>> loginStateFlow = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlinx.coroutines.flow.StateFlow<com.touchalife.talradio.data.network.Resource<com.touchalife.talradio.data.responses.LoginResponse>> _loginStateFlow = null;
    private final kotlinx.coroutines.flow.MutableStateFlow<com.touchalife.talradio.data.network.Resource<com.touchalife.talradio.data.responses.SocialLoginResponse>> socialLoginStateFlow = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlinx.coroutines.flow.StateFlow<com.touchalife.talradio.data.network.Resource<com.touchalife.talradio.data.responses.SocialLoginResponse>> _socialLoginStateFlow = null;
    
    @javax.inject.Inject()
    public AuthViewModel(@org.jetbrains.annotations.NotNull()
    com.touchalife.talradio.data.repository.AuthRepository repository) {
        super(null);
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.StateFlow<com.touchalife.talradio.data.network.Resource<com.touchalife.talradio.data.responses.LoginResponse>> get_loginStateFlow() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.StateFlow<com.touchalife.talradio.data.network.Resource<com.touchalife.talradio.data.responses.SocialLoginResponse>> get_socialLoginStateFlow() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job login(@org.jetbrains.annotations.NotNull()
    java.lang.String email, @org.jetbrains.annotations.NotNull()
    java.lang.String password) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job socialLogin(@org.jetbrains.annotations.Nullable()
    java.lang.String personId, @org.jetbrains.annotations.Nullable()
    java.lang.String idType, @org.jetbrains.annotations.Nullable()
    java.lang.String verifyType, @org.jetbrains.annotations.Nullable()
    java.lang.String email, @org.jetbrains.annotations.Nullable()
    java.lang.String displayName, @org.jetbrains.annotations.Nullable()
    java.lang.String provider) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object saveAccessTokens(@org.jetbrains.annotations.NotNull()
    java.lang.String accessToken, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
}