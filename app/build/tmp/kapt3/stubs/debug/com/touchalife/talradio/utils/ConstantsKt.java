package com.touchalife.talradio.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0005\"\u001a\u0010\u0000\u001a\u00020\u0001X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0002\u0010\u0003\"\u0004\b\u0004\u0010\u0005\"\u001a\u0010\u0006\u001a\u00020\u0001X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\u0003\"\u0004\b\b\u0010\u0005\"\u001a\u0010\t\u001a\u00020\u0001X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0003\"\u0004\b\u000b\u0010\u0005\"\u001a\u0010\f\u001a\u00020\u0001X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u0003\"\u0004\b\u000e\u0010\u0005\"\u001a\u0010\u000f\u001a\u00020\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014\u00a8\u0006\u0015"}, d2 = {"FACEBOOK", "", "getFACEBOOK", "()Ljava/lang/String;", "setFACEBOOK", "(Ljava/lang/String;)V", "GOOGLE", "getGOOGLE", "setGOOGLE", "LINKEDIN", "getLINKEDIN", "setLINKEDIN", "TWITTER", "getTWITTER", "setTWITTER", "emailPattern", "Lkotlin/text/Regex;", "getEmailPattern", "()Lkotlin/text/Regex;", "setEmailPattern", "(Lkotlin/text/Regex;)V", "app_debug"})
public final class ConstantsKt {
    @org.jetbrains.annotations.NotNull()
    private static kotlin.text.Regex emailPattern;
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String GOOGLE = "google";
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String FACEBOOK = "facebook";
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String LINKEDIN = "linked in";
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String TWITTER = "twitter";
    
    @org.jetbrains.annotations.NotNull()
    public static final kotlin.text.Regex getEmailPattern() {
        return null;
    }
    
    public static final void setEmailPattern(@org.jetbrains.annotations.NotNull()
    kotlin.text.Regex p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getGOOGLE() {
        return null;
    }
    
    public static final void setGOOGLE(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getFACEBOOK() {
        return null;
    }
    
    public static final void setFACEBOOK(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getLINKEDIN() {
        return null;
    }
    
    public static final void setLINKEDIN(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getTWITTER() {
        return null;
    }
    
    public static final void setTWITTER(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
}