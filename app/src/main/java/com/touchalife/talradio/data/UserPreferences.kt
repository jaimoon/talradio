package com.touchalife.talradio.data

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "tal_radio_data_store")

class UserPreferences @Inject constructor(@ApplicationContext context: Context) {

    private val appContext = context.applicationContext


    val accessToken: Flow<String?>
        get() = appContext.dataStore.data.map { preferences ->
            preferences[ACCESS_TOKEN]
        }


    val name: Flow<String?>
        get() = appContext.dataStore.data.map { preferences ->
            preferences[NAME]
        }

    val gender: Flow<String?>
        get() = appContext.dataStore.data.map { preferences ->
            preferences[GENDER]
        }

    val avatar: Flow<String?>
        get() = appContext.dataStore.data.map { preferences ->
            preferences[AVATAR]
        }

    val profilePic: Flow<String?>
        get() = appContext.dataStore.data.map { preferences ->
            preferences[PROFILE_PIC]
        }

    val hugPic: Flow<String?>
        get() = appContext.dataStore.data.map { preferences ->
            preferences[HUG_PIC]
        }

    val greetings: Flow<String?>
        get() = appContext.dataStore.data.map { preferences ->
            preferences[GREETINGS]
        }


    val loginStatus: Flow<Boolean>
        get() = appContext.dataStore.data.map { preferences ->
            preferences[LOGIN_STATUS] ?: false
        }


    suspend fun loginStatus(isNightMode: Boolean) {
        appContext.dataStore.edit { preferences ->
            preferences[LOGIN_STATUS] = isNightMode
        }
    }

    suspend fun saveAccessTokens(accessToken: String) {
        appContext.dataStore.edit { preferences ->
            preferences[ACCESS_TOKEN] = accessToken

        }
    }

    suspend fun saveUserData(
        name: String,
        gender: String,
        avatar: String,
        profilePic: String,
        hugPic: String,
        greetings: String
    ) {
        appContext.dataStore.edit { preferences ->
            preferences[NAME] = name
            preferences[GENDER] = gender
            preferences[AVATAR] = avatar
            preferences[PROFILE_PIC] = profilePic
            preferences[HUG_PIC] = hugPic
            preferences[GREETINGS] = greetings

        }
    }

    suspend fun clear() {
        appContext.dataStore.edit { preferences ->
            preferences.clear()
        }
    }

    companion object {
        private val ACCESS_TOKEN = stringPreferencesKey("key_access_token")
        private val NAME = stringPreferencesKey("name")
        private val GENDER = stringPreferencesKey("gender")
        private val AVATAR = stringPreferencesKey("avatar")
        private val PROFILE_PIC = stringPreferencesKey("profilePic")
        private val HUG_PIC = stringPreferencesKey("hugPic")
        private val GREETINGS = stringPreferencesKey("greeting")

        private val LOGIN_STATUS = booleanPreferencesKey("login_status")

    }


}