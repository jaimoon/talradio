package com.touchalife.talradio.data.network

import com.google.gson.JsonObject
import com.touchalife.talradio.data.responses.*
import okhttp3.ResponseBody
import retrofit2.http.*

interface Api  {

    @POST("login")
    suspend fun login(
       @Body jsonObject: JsonObject,
    ): LoginResponse


    @POST("device/oautherize")
    suspend fun socialLogin(
        @Body jsonObject: JsonObject,
    ): SocialLoginResponse




















    @GET("posts")
    suspend fun getPost():List<Post>

    @Headers("Content-Type: application/json")
    @POST("validateAccessCode")
    suspend fun validateAccessCode(
        @Body jsonObject: JsonObject,
    ): AccessCodeResponse


    @GET("user")
    suspend fun getUser(): LoginResponse

    @FormUrlEncoded
    @POST("auth/refresh-token")
    suspend fun refreshAccessToken(
        @Field("refresh_token") refreshToken: String?
    ): TokenResponse

    @POST("logout")
    suspend fun logout(): ResponseBody





}
