package com.touchalife.talradio.data.network

import com.touchalife.talradio.utils.Status
import okhttp3.ResponseBody

sealed class Resource<out T> (val status : Status, val _data : T?, val message: String?) {

    data class Success<out R>(val data: R) : Resource<R>(
        status = Status.SUCCESS,
        _data = data,
        message = null
    )

    object Loading : Resource<Nothing>(
        status = Status.LOADING,
        _data = null,
        message = null
    )

    data class Error(val exception: String) : Resource<Nothing>(
        status = Status.ERROR,
        _data = null,
        message = exception
    )

}