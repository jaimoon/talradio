package com.touchalife.talradio.data.repository

import com.google.gson.JsonObject
import com.touchalife.talradio.data.UserPreferences
import com.touchalife.talradio.data.network.Api
import com.touchalife.talradio.data.responses.LoginResponse
import com.touchalife.talradio.data.responses.SocialLoginResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class AuthRepository @Inject constructor(
    private val api: Api,
    private val preferences: UserPreferences
) : BaseRepository(api) {


    // normal Login
    suspend fun getLogin(jsonObject: JsonObject): Flow<LoginResponse> {
        return flow {
            val loginData = api.login(
                jsonObject
            )
            emit(loginData)

        }.flowOn(Dispatchers.IO)
    }


    // social Login
    suspend fun getSocialLogin(jsonObject: JsonObject): Flow<SocialLoginResponse> {
        return flow {
            val socialLoginData = api.socialLogin(
                jsonObject
            )
            emit(socialLoginData)

        }.flowOn(Dispatchers.IO)
    }


    suspend fun saveAccessTokens(accessToken: String) {
        preferences.saveAccessTokens(accessToken)
    }
}