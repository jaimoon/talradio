package com.touchalife.talradio.data.repository

import com.touchalife.talradio.data.network.Api
import com.touchalife.talradio.data.network.SafeApiCall


abstract class BaseRepository(private val api: Api) : SafeApiCall {

    suspend fun logout() = safeApiCall {
        api.logout()
    }
}