package com.touchalife.talradio.data.repository

import com.touchalife.talradio.data.network.Api

import javax.inject.Inject

class UserRepository @Inject constructor(
    private val api: Api
) : BaseRepository(api) {

    suspend fun getUser() = safeApiCall { api.getUser() }

}