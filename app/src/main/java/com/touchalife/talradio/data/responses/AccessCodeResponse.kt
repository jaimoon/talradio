package com.touchalife.talradio.data.responses


import com.google.gson.annotations.SerializedName

data class AccessCodeResponse(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("message")
    val message: String,
    @SerializedName("success")
    val success: Boolean
) {
    data class Data(
        @SerializedName("greeting")
        val greeting: Any,
        @SerializedName("hugPic")
        val hugPic: Any,
        @SerializedName("profilePic")
        val profilePic: Any,
        @SerializedName("refreshToken")
        val refreshToken: String,
        @SerializedName("token")
        val token: String,
        @SerializedName("user")
        val user: User
    ) {
        data class User(
            @SerializedName("avatar")
            val avatar: Any,
            @SerializedName("gender")
            val gender: Any,
            @SerializedName("name")
            val name: Any
        )
    }
}