package com.touchalife.talradio.di

import android.app.Application
import com.facebook.FacebookSdk
import dagger.hilt.android.HiltAndroidApp
import com.facebook.appevents.AppEventsLogger

@HiltAndroidApp
class MyApplication : Application(){

}