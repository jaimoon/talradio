package com.touchalife.talradio.ui

import android.R
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.android.material.snackbar.Snackbar
import com.touchalife.talradio.data.network.Resource
import com.touchalife.talradio.databinding.ActivityLoginBinding
import com.touchalife.talradio.ui.viewmodel.AuthViewModel
import com.touchalife.talradio.utils.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.util.*


@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {

    lateinit var callbackManager: CallbackManager
    private lateinit var binding: ActivityLoginBinding
    private val viewModels by viewModels<AuthViewModel>()

    /* @Inject
     lateinit var postAdapter: PostAdapter*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)




        // initRecyclerview()


        binding.infoImage.setOnClickListener {

            val builder = AlertDialog.Builder(this@LoginActivity)
            builder.setTitle(getString(com.touchalife.talradio.R.string.app_name))
                .setMessage("You can use your TALGiving credentials to login here")
                .setPositiveButton(
                    R.string.yes
                ) { dialog, _ -> // continue with delete
                    dialog.cancel()
                }
                .show()

        }

        binding.login.setOnClickListener {
            if (validLogin()) {

                // viewModels.login("jayachandra.k@touchalife.org", "Solix@4046")

                viewModels.login(binding.email.text.toString(), binding.password.text.toString())
                lifecycleScope.launchWhenStarted {
                    viewModels._loginStateFlow.collect { it ->
                        when (it) {

                            is Resource.Loading -> {

                                setProgressDialog(this@LoginActivity, true)

                            }

                            is Resource.Error -> {

                                setProgressDialog(this@LoginActivity, false)


                                Snackbar.make(
                                    binding.root,
                                    it.exception,
                                    Snackbar.LENGTH_SHORT
                                ).show()

                            }
                            is Resource.Success -> {

                                setProgressDialog(this@LoginActivity, false)


                                if (it.data.statusCode == 200) {

                                    lifecycleScope.launch {
                                        viewModels.saveAccessTokens(it.data.data.tokenDetail.token)
                                        startClearActivity(HomeActivity::class.java)
                                        toast("SignIn Successfully")
                                    }

                                } else {
                                    Snackbar.make(
                                        binding.root,
                                        it.data.message,
                                        Snackbar.LENGTH_SHORT
                                    ).show()
                                }

                                // postAdapter.submitList(it.data)
                            }
                        }
                    }
                }

            }
            hideKeyboard()

        }

        binding.googleLayout.setOnClickListener {

            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()

            val mGoogleSignInClient = GoogleSignIn.getClient(this, gso)

            val signInIntent = mGoogleSignInClient.signInIntent
            launchGoogleActivity.launch(signInIntent)

        }

        binding.facebookLayout.setOnClickListener {
            callbackManager = CallbackManager.Factory.create()
            LoginManager.getInstance().logInWithReadPermissions(this, listOf("public_profile", "email"));
            LoginManager.getInstance().registerCallback(callbackManager,
                object : FacebookCallback<LoginResult> {
                    override fun onSuccess(loginResult: LoginResult) {
                        Log.e("LoginResult2 1:", loginResult.accessToken.toString())
                        setFacebookData(loginResult)
                    }

                    override fun onCancel() {
                        // App code
                        println("LoginResult2 : cancel :::")
                    }

                    override fun onError(exception: FacebookException) {
                        // App code
                        println(" LoginResult2 : exception :::")
                    }
                })
            binding.login.callOnClick()

        }

        binding.register.setOnClickListener {
            startNewActivity(RegistrationActivity::class.java)
        }

        binding.reset.setOnClickListener {
            startNewActivity(ResetActivity::class.java)
        }

        binding.headerBack.setOnClickListener {
            finish()
        }


    }

    private fun setFacebookData(loginResult: LoginResult) {

        val request = GraphRequest.newMeRequest(
            loginResult.accessToken
        ) { `object`, _ -> // Application code
            Log.i("Response", `object`.toString())
            try {
                socialLogin(
                    `object`.optString("id"), "facebook_id", "facebook_verified", `object`.optString("email"),
                    `object`.optString("first_name") + `object`.optString("last_name"), FACEBOOK
                )
            } catch (e: Exception) {
                Log.w("fb profile data", "data ", e)
            }
        }
        val parameters = Bundle()
        parameters.putString("fields", "id,email,first_name,last_name")
        request.parameters = parameters
        request.executeAsync()
    }

    // result from google
    private var launchGoogleActivity =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data

                val task: Task<GoogleSignInAccount> =
                    GoogleSignIn.getSignedInAccountFromIntent(data)
                handleSignInResult(task)

            } else if (result.resultCode == Activity.RESULT_CANCELED) {
                Log.d(TAG, "CANCELLED :  " + result.describeContents())
            }
        }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account: GoogleSignInAccount = completedTask.getResult(ApiException::class.java)

            Log.d(TAG, "handleSignInResult: " + account.displayName)
            // Signed in successfully, show authenticated UI.

            socialLogin(
                account.id,
                "google_id",
                "google_verified",
                account.email,
                account.displayName,
                GOOGLE
            )

        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.statusCode)
            //  updateUI(null)
        }
    }


    private fun socialLogin(
        personId: String?,
        idType: String?,
        verifyType: String?,
        email: String?,
        displayName: String?,
        provider: String?
    ) {

        when (provider) {
            GOOGLE -> {
                viewModels.socialLogin(
                    personId,
                    idType,
                    verifyType,
                    email,
                    displayName,
                    provider
                )
            }
            FACEBOOK -> {
                viewModels.socialLogin(
                    personId,
                    idType,
                    verifyType,
                    email,
                    displayName,
                    provider
                )
            }
            LINKEDIN -> {
                viewModels.socialLogin(
                    personId,
                    idType,
                    verifyType,
                    email,
                    displayName,
                    provider
                )
            }
            TWITTER -> {
                viewModels.socialLogin(
                    personId,
                    idType,
                    verifyType,
                    email,
                    displayName,
                    provider
                )
            }
        }
        lifecycleScope.launchWhenStarted {
            viewModels._loginStateFlow.collect { it ->
                when (it) {

                    is Resource.Loading -> {

                        setProgressDialog(this@LoginActivity, true)
                    }

                    is Resource.Error -> {

                        setProgressDialog(this@LoginActivity, false)
                        Snackbar.make(
                            binding.root,
                            it.exception,
                            Snackbar.LENGTH_SHORT
                        ).show()

                    }
                    is Resource.Success -> {

                        setProgressDialog(this@LoginActivity, false)

                        if (it.data.statusCode == 200) {

                            lifecycleScope.launch {
                                viewModels.saveAccessTokens(it.data.data.tokenDetail.token)
                                startClearActivity(MainActivity::class.java)

                                toast("SignIn Successfully")

                            }

                        } else {
                            Snackbar.make(
                                binding.root,
                                it.data.message,
                                Snackbar.LENGTH_SHORT
                            ).show()
                        }

                        // postAdapter.submitList(it.data)
                    }
                }
            }
        }
    }


    /* private fun bindingProgress(
         isVisibleProgress: Boolean,
         isVisibleRecycler: Boolean,
     ) {
         binding.apply {
             progressBar.isVisible = isVisibleProgress
             recyclerView.isVisible = isVisibleRecycler

         }
     }

     private fun initRecyclerview() {
         binding.apply {
             recyclerView.apply {
                 setHasFixedSize(true)
                 layoutManager = LinearLayoutManager(this@LoginActivity)
                 adapter = postAdapter
             }
         }
     }*/

    private fun validLogin(): Boolean {

        if (binding.email.text.toString().isEmpty()) {
            binding.emailErr.visibility = View.VISIBLE
            return false
        } else if (!binding.email.text.toString().trim().matches(emailPattern)) {
            binding.emailErr.visibility = View.VISIBLE
            binding.emailErr.text = "Invalid email address"
            return false
        } else {
            binding.emailErr.visibility = View.GONE
        }


        if (binding.password.text.toString().isEmpty()) {
            binding.pwdErr.visibility = View.VISIBLE
            return false
        } else if (binding.password.text.toString().length < 8) {
            binding.pwdErr.visibility = View.VISIBLE
            binding.pwdErr.text = "Please enter minimum 8 char password"
            return false
        } else {
            binding.pwdErr.visibility = View.GONE

        }
        return true
    }


    companion object {
        const val TAG = "LoginActivity"
    }
}