package com.touchalife.talradio.ui

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import com.touchalife.talradio.R
import com.touchalife.talradio.databinding.ActivityRegistrationBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RegistrationActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRegistrationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegistrationBinding.inflate(layoutInflater)
        setContentView(binding.root)


        val first = "By clicking Register, I agree that I have read and accepted the "
        val next = "<font color='#cc2626'>Terms of Use.</font>"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            binding.termsText.text = Html.fromHtml(first+next, Html.FROM_HTML_MODE_COMPACT);
        } else {
            binding.termsText.text = Html.fromHtml(first+next);
        }

    }
}