package com.touchalife.talradio.ui

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.lifecycle.Observer
import androidx.lifecycle.asLiveData
import androidx.lifecycle.lifecycleScope
import com.touchalife.talradio.data.UserPreferences
import com.touchalife.talradio.databinding.ActivitySplashBinding
import com.touchalife.talradio.utils.startNewActivity
import kotlinx.coroutines.launch


@SuppressLint("CustomSplashScreen")
class SplashActivity : AppCompatActivity() {

    lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val userPreferences = UserPreferences(this)

        Handler(Looper.getMainLooper()).postDelayed({

            userPreferences.accessToken.asLiveData().observe(this, {
                val activity =
                    if (it == null) LoginActivity::class.java else HomeActivity::class.java
                startNewActivity(activity)
            })
            finish()

        }, 1000)

    }
}