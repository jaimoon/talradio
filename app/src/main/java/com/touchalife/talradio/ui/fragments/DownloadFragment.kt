package com.touchalife.talradio.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.touchalife.talradio.R
import com.touchalife.talradio.databinding.FragmentDownloadBinding
import com.touchalife.talradio.databinding.FragmentGuideBinding
import com.touchalife.talradio.ui.viewmodel.DownloadViewModel
import com.touchalife.talradio.ui.viewmodel.FeedbackViewModel


class DownloadFragment : Fragment() {
    private lateinit var downloadViewModel : DownloadViewModel
    private var _binding: FragmentDownloadBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        downloadViewModel =
            ViewModelProvider(this).get(DownloadViewModel::class.java)

        _binding = FragmentDownloadBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val textView: TextView = binding.textHome
        downloadViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}