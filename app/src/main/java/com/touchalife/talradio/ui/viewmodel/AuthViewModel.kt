package com.touchalife.talradio.ui.viewmodel

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import com.touchalife.talradio.data.network.NoConnectivityException
import com.touchalife.talradio.data.network.Resource
import com.touchalife.talradio.data.repository.AuthRepository
import com.touchalife.talradio.data.responses.LoginResponse
import com.touchalife.talradio.data.responses.SocialLoginResponse
import com.touchalife.talradio.ui.LoginActivity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.json.JSONObject
import javax.inject.Inject

@HiltViewModel
class AuthViewModel
@Inject constructor(
    private val repository: AuthRepository
) : BaseViewModel(repository) {

    private val loginStateFlow: MutableStateFlow<Resource<LoginResponse>> =
        MutableStateFlow(Resource.Loading)
    val _loginStateFlow: StateFlow<Resource<LoginResponse>> = loginStateFlow

    private val socialLoginStateFlow: MutableStateFlow<Resource<SocialLoginResponse>> =
        MutableStateFlow(Resource.Loading)
    val _socialLoginStateFlow: StateFlow<Resource<SocialLoginResponse>> = socialLoginStateFlow


    // login
    fun login(email: String, password: String) = viewModelScope.launch {
        loginStateFlow.value = Resource.Loading

        // json raw Data
        val jsonObject = JsonObject()
        jsonObject.addProperty("account", email)
        jsonObject.addProperty("password", password)


        repository.getLogin(jsonObject)
            .catch { e ->
                if (e is NoConnectivityException) {
                    // show No Connectivity message to user or do whatever you want.
                    loginStateFlow.value = Resource.Error(e.message)

                } else {
                    loginStateFlow.value = Resource.Error(e.message.toString())

                }
            }.collect { data ->
                loginStateFlow.value = Resource.Success(data)
            }
    }


    // socialLogin
    fun socialLogin(personId: String?, idType: String?,verifyType: String?, email: String?,displayName: String?, provider: String?) = viewModelScope.launch {
        socialLoginStateFlow.value = Resource.Loading

        // json raw Data
        val jsonObject = JsonObject()
        jsonObject.addProperty("id", personId)
        jsonObject.addProperty("id_type", idType)
        jsonObject.addProperty("verify_type", verifyType)
        jsonObject.addProperty("email", email)
        jsonObject.addProperty("displayName", displayName)
        jsonObject.addProperty("provider", provider)

        repository.getSocialLogin(jsonObject)
            .catch { e ->
                if (e is NoConnectivityException) {
                    // show No Connectivity message to user or do whatever you want.
                    socialLoginStateFlow.value = Resource.Error(e.message)

                } else {
                    socialLoginStateFlow.value = Resource.Error(e.message.toString())

                }
            }.collect { data ->
                socialLoginStateFlow.value = Resource.Success(data)
            }
    }

    suspend fun saveAccessTokens(accessToken: String) {
        repository.saveAccessTokens(accessToken)
    }

}

