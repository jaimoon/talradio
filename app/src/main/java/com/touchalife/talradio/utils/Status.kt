package com.touchalife.talradio.utils

enum class Status {
    LOADING,
    SUCCESS,
    ERROR
}